package main;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

/**
 *
 *
 * ChatControllerUI.java
 *
 *
 * @author c0der
 * 24 Aug 2021
 *
 */
public class ChatControllerUI {

	private final Model model;
	private final JToggleButton serverBtn;
	private final JButton  addClientBtn;
	private final JTextField serverStateTf;

	ChatControllerUI(Model model){

		this.model = model;

		JFrame frame=new JFrame("Chat Controller");
		frame.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				serverOff();
			}
		});

		serverBtn = new JToggleButton();
		serverBtn.addActionListener(e -> toggleServer());

		serverStateTf = new  JTextField(20);

		JPanel topPane = new JPanel();
		topPane.setLayout(new BoxLayout(topPane, BoxLayout.LINE_AXIS));
		topPane.add(serverStateTf);
		topPane.add(Box.createHorizontalGlue());
		topPane.add(serverBtn);
		frame.add(topPane, BorderLayout.NORTH);

		JCheckBox demoMode = new JCheckBox("Run in demo mode");
		demoMode.addChangeListener(e -> model.setDemoMode(demoMode.isSelected()));

		addClientBtn = new JButton(" Add Client   ");
		addClientBtn.addActionListener(e -> addClient());

		JPanel bottomPane = new JPanel();
		bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.LINE_AXIS));
		bottomPane.add(demoMode);
		bottomPane.add(Box.createHorizontalGlue());
		bottomPane.add(addClientBtn);
		frame.add(bottomPane, BorderLayout.SOUTH);

		serverOff();
		serverBtn.setEnabled(true);

		int randoLocation = new Random().nextInt(250);
		frame.setLocation(randoLocation, randoLocation );
		frame.pack();
		frame.setVisible(true);
	}

	private void toggleServer(){
		if(!serverBtn.isSelected()){
			serverOff();
		}else{
			serverOn();
		}
	}

	private void serverOff(){
		model.setServerOn(false);
		serverBtn.setText("Start Server");
        serverStateTf.setText("Server is off");
		addClientBtn.setEnabled(false);
		serverBtn.setEnabled(false);
	}

    private void serverOn(){
    	model.setServerOn(true);
    	serverBtn.setText("Stop Server");
		serverStateTf.setText("Server is on");
		addClientBtn.setEnabled(true);
	}

	private void addClient() {
		model.addClientRequested();
	}
}

