/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import client.Client;
import client.ClientModel;
import server.Server;

/**
 *
 *
 * ChatController.java
 *
 *
 * @author c0der
 * 24 Aug 2021
 *
 */
public class ChatController {

	private final static int PORT_NUMBER = 8000;
	private final static String HOST_NAME = "localhost";
	private final Model model;
	private final Server server;

	public ChatController() {

		model = new Model(HOST_NAME, PORT_NUMBER);
		model.addPropertyChangeListener(evt->{
			if (evt.getPropertyName().equals(Model.SERVER_ON)) {
				  serverOn();
			}else if (evt.getPropertyName().equals(Model.SERVER_OFF)) {
				  serverOff();
			}else if (evt.getPropertyName().equals(Model.ADD_CLIENT)) {
				  addClient();
			}else if(evt.getPropertyName().equals(Model.MESSAGE_ADDED)){
				updateClients(evt.getNewValue());
			}
		});

		server = new Server(model);
		new ChatControllerUI(model);
	}

	private void serverOff(){
		showMessageToAllClients("Server is off");
		disableAllClients();
		server.stop();
	}

    private void serverOn(){
		server.start();
		showMessageToAllClients("Server is on");
	}

	private void addClient() {

		ClientModel clientModel = new ClientModel();
		clientModel.setMessageToUser(model.isServerOn() ? "Server is on" : "Server is off");

		Client client  = new Client(model,clientModel);
		if(model.isDemoMode()){
			client.setDemoMode(true);
		}

		client.start();

		model.addClient(clientModel);

		clientModel.addPropertyChangeListener(evt->{
			if (evt.getPropertyName().equals(ClientModel.SHUT) && clientModel.isShutDown()) {
				model.removeClient(clientModel);
			}
		});
	}

	private void updateClients(Object newValue) {
		if(! (newValue instanceof String)) return;
		String 	message = (String) newValue;

		for(ClientModel clientModel : model.getClients()){
			clientModel.addMessage(message);
		}
	}

	private void showMessageToAllClients(String msg){
		for(ClientModel clientModel : model.getClients()){
			clientModel.setMessageToUser(msg);
		}
	}

	private void clearMessageToAllClients(){
		showMessageToAllClients("");
	}

	private void disableAllClients(){
		for(ClientModel clientModel : model.getClients()){
			clientModel.setDisabled(true);
		}
	}

	public static void main(String[] args) {
		new ChatController();
	}
}
