package main;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import client.ClientModel;

/**
 *
 *
 * Model.java
 *
 *
 * @author c0der
 * 24 Aug 2021
 *
 */
public class Model{

	public static final String MESSAGE_ADDED = "message added to Server", SERVER_ON = "turn server on",
			SERVER_OFF = "turn server off", ADD_CLIENT = "add client request" ;

	/**
	 * Record all messages
	 * todo: set limit and clear
	 */
	private final List<String> messages;

	private final String hostName;
    private final int postNumber;
	private boolean isServerOn, isDemoMode;
	private final List<ClientModel> clients;

	/**
	 * Support propertyChange
	 */
	private final PropertyChangeSupport propertyChangeSupport;

	public Model(String hostName, int postNumber) {
		this.hostName = hostName;
		this.postNumber = postNumber;
		propertyChangeSupport = new PropertyChangeSupport(this);
		messages = new ArrayList<>();
		clients = new ArrayList<>();
		isServerOn = false; isDemoMode = false;
	}

	public synchronized void addMessage(String message){
		messages.add(message);
		//notify listeners
		propertyChangeSupport.firePropertyChange(MESSAGE_ADDED, null, message);
	}

	/**
	 * Add listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * Remove listeners
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public String getHostName() {
		return hostName;
	}

	public int getPostNumber() {
		return postNumber;
	}

	public void addClientRequested(){
	  propertyChangeSupport.firePropertyChange(ADD_CLIENT, null, true);
	}

	void addClient(ClientModel clientModel){
		clients.add(clientModel);
	}

	void removeClient(ClientModel clientModel){
		clients.remove(clientModel);
	}

	List<ClientModel> getClients(){
		return new ArrayList<>(clients); //return a copy
	}

	public boolean isServerOn() {
		return isServerOn;
	}

	void setServerOn(boolean isServerOn) {
		boolean oldValue = this.isServerOn;
		this.isServerOn = isServerOn;
		propertyChangeSupport.firePropertyChange(isServerOn ? SERVER_ON : SERVER_OFF, oldValue, isServerOn);
	}

	public boolean isDemoMode() {
		return isDemoMode;
	}

	public void setDemoMode(boolean isDemoMode) {
		this.isDemoMode = isDemoMode;
	}
}
