package client;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import main.Model;

/**
 *
 *
 * ClientUI.java
 *
 *
 * @author c0der
 * 24 Aug 2021
 *
 */
public class ClientUI {

	private JTextArea chatServerArea;
	private JTextField clientMessageTf, systemMessageTf;
	private ClientModel clientModel;
	private JScrollPane scrollServerArea;
	private JFrame frame;
	private JButton sendBtn;

	ClientUI(ClientModel clientModel, Model model){

		this.clientModel = clientModel;

		if(clientModel.getClientName() == null){
			clientModel.setClientName(model.isDemoMode() ? generateRandomName() : getNameFromUser());
		}

		if(clientModel.getClientName() == null){//user did not enter name
			shutDown();
			return;
		}

		clientModel.addPropertyChangeListener(evt->{
			if(evt.getPropertyName().equals(ClientModel.MESSAGE_TO_USER)){
				systemMessageTf.setText(clientModel.getMessageToUser());
			}else if(evt.getPropertyName().equals(ClientModel.SERVER_MESSAGE_ADDED)){
				SwingUtilities.invokeLater(()-> updateFromServer(evt.getNewValue()));
			}else if(evt.getPropertyName().equals(ClientModel.DISABLE)){
				disable();
			}
		});

		frame = new JFrame(clientModel.getClientName());//creating instance of JFrame
		frame.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				shutDown();
			}
		});

		systemMessageTf = new  JTextField(clientModel.getMessageToUser());
		systemMessageTf.setColumns(26);

		JPanel systemMessagePane = new JPanel();
		systemMessagePane.add(systemMessageTf);
		frame.getContentPane().add(systemMessagePane, BorderLayout.NORTH);

		chatServerArea=new JTextArea(10,30);  //messages are displayed here
		chatServerArea.setEditable(false);
		scrollServerArea = new JScrollPane(chatServerArea);
		frame.getContentPane().add(scrollServerArea);

		clientMessageTf= new  JTextField(20);
		JScrollPane scrolClientMessage=new JScrollPane(clientMessageTf);

		sendBtn = new JButton("Send");// send button to send the messages
		sendBtn.addActionListener(e->sendMessage());
		frame.getRootPane().setDefaultButton(sendBtn);

		JPanel bottomPane = new JPanel();
		bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.LINE_AXIS));
		bottomPane.add(scrolClientMessage);
		bottomPane.add(Box.createHorizontalGlue());
		bottomPane.add(sendBtn);
		frame.getContentPane().add(bottomPane, BorderLayout.SOUTH);

		int randoLocation = new Random().nextInt(250);
		frame.setLocation(randoLocation, randoLocation );
		frame.pack();
		frame.setVisible(true);

		runDemo(40);
	}

	private String generateRandomName() {
		return "client "+ new Random().nextInt(1000);
	}

	private String getNameFromUser() {
		while(true){
			String clientName = JOptionPane.showInputDialog("To start chat enter your name ");

			if(clientName != null && clientName.trim().isEmpty()) {
				Toolkit.getDefaultToolkit().beep(); //beep
			} else
				return clientName;
		}
	}

	private void shutDown() {

		clientModel.setShutdown(true);
		if(frame != null) {
			frame.dispose();
		}
	}

	private void disable(){
		sendBtn.setEnabled(false);
	}

	private void updateFromServer(Object newValue) {
		if(newValue instanceof String) {
			chatServerArea.append("\n"+(String) newValue);
			JScrollBar vertical = scrollServerArea.getVerticalScrollBar();
			vertical.setValue( vertical.getMaximum() );//scroll down
		}
	}

	private void sendMessage() {
		String text = clientMessageTf.getText();
		if(! text.trim().isEmpty()){//do not sent empty messages
			clientModel.sendMessage(text);
			clientMessageTf.setText("");
		}
	}

	private void runDemo(int durationInSeconds){

		Random rnd = new Random();
		long start = System.currentTimeMillis();
		new Thread(()->{
			while( !clientModel.isDisabled() && clientModel.isDemoMode() &&
						System.currentTimeMillis() - start < durationInSeconds*1000 ){
				long sleepTime = 3500 + rnd.nextInt(5000); //random time between messages
				String message = UUID.randomUUID().toString() ; //random text
				try {
					SwingUtilities.invokeLater(()->{
						clientMessageTf.setText(message);
						sendMessage();
					});
					TimeUnit.MILLISECONDS.sleep(sleepTime);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}).start();
	}
}

