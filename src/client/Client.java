package client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import main.Model;
/**
 *
 *
 * Client.java
 *
 *
 * @author c0der
 * 24 Aug 2021
 *
 */
public class Client{

	private String clientName;
	private final Model model;
	private final ClientModel clientModel;
	private DataOutputStream out;
	private boolean isDemoMode;

	public Client(Model model, ClientModel clientModel)  {
		this.model = model;
		this.clientModel = clientModel;
		isDemoMode = false;
		clientModel.setShutdown(false);
	}

	private void runClient(){

		Socket socket = null;
		try {
			socket = new Socket(model.getHostName(), model.getPostNumber());
			out = new DataOutputStream( socket.getOutputStream() );

			clientModel.setClientName(clientName).setDemoMode(isDemoMode);
			clientModel.addPropertyChangeListener(evt->{
				if(evt.getPropertyName().equals(ClientModel.CLIENT_MESSAGE_ADDED)) {
					send(evt.getNewValue());
				}
			});

			new ClientUI(clientModel, model);

		} catch (UnknownHostException e) {
			e.printStackTrace();
			clientModel.setShutdown(true);
		} catch (IOException e) {
			e.printStackTrace();
			clientModel.setShutdown(true);
		}
	}

	private void send(Object message) {
		if(message instanceof String) {
			try {
				out.writeUTF((String) message);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public void start(){
		new Thread(()->runClient()).start();
	}

	public boolean isDemoMode() {
		return isDemoMode;
	}

	public Client setDemoMode(boolean isDemoMode) {
		this.isDemoMode = isDemoMode;
		return this;
	}

	public String getClientName() {
		return clientName;
	}

	public Client setClientName(String clientName) {
		this.clientName = clientName;
		return this;
	}
}