package client;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 *
 * ClientModel.java
 *
 *
 * @author c0der
 * 24 Aug 2021
 *
 */
public class ClientModel{

	public static final String CLIENT_MESSAGE_ADDED = "client added message", SHUT ="shut down client", DISABLE ="disable client",
							   MESSAGE_TO_USER = "messae to user", SERVER_MESSAGE_ADDED = "message added from Server";
	private String clientName, messageToUser = "";
	private boolean isDemoMode, isShutDown, isDisabled;

	/**
	 * Support propertyChange
	 */
	private final PropertyChangeSupport propertyChangeSupport;

	public ClientModel(){
		propertyChangeSupport = new PropertyChangeSupport(this);
		isDemoMode = false; isShutDown = true; isDisabled = false;
	}

	void sendMessage(String message){
		StringBuilder sb = new StringBuilder().append("[").append(clientName).append("] : ").append(message);
		propertyChangeSupport.firePropertyChange(CLIENT_MESSAGE_ADDED, null, sb.toString());
	}

	public  void addMessage(String message){
		//notify listeners
		propertyChangeSupport.firePropertyChange(SERVER_MESSAGE_ADDED, null, message);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public String getClientName() {
		return clientName;
	}

	public ClientModel setClientName(String clientName) {
		this.clientName = clientName;
		return this;
	}

	public boolean isDemoMode() {
		return isDemoMode;
	}

	public ClientModel setDemoMode(boolean isDemoMode) {
		this.isDemoMode = isDemoMode;
		return this;
	}

	public boolean isShutDown() {
		return isShutDown;
	}

	public ClientModel setShutdown(boolean isShutDown) {
		boolean oldValue = this.isShutDown;
		this.isShutDown = isShutDown;
		propertyChangeSupport.firePropertyChange(SHUT, oldValue, isShutDown);
		return this;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public ClientModel setDisabled(boolean isDisabled) {
		boolean oldValue = this.isDisabled;
		this.isDisabled = isDisabled;
		propertyChangeSupport.firePropertyChange(DISABLE, oldValue, isDisabled);
		return this;
	}

	public void setMessageToUser(String msg) {
		messageToUser = msg;
		propertyChangeSupport.firePropertyChange(MESSAGE_TO_USER, null, msg);
	}

	public String getMessageToUser() {
		return messageToUser;
	}
}
