package server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import main.Model;

/**
 *
 *
 * Server.java
 *
 *
 * @author c0der
 * 24 Aug 2021
 *
 */
public class Server {

	private final ExecutorService pool;
	private  boolean stop;
	private ServerSocket serverSocket;
	private final Model model;
	private final List<ServerThread> clients;

	public Server(Model model) {
		this.model = model;
		pool = Executors.newFixedThreadPool(3);
		clients = new ArrayList<>();
	}

	private void runServer(){

		System.out.println("SERVER: Waiting for client");
		try{
			serverSocket = new ServerSocket(model.getPostNumber());
			stop = false;

			while(! stop){//do in loop to support multiple clients
				//expected to throw an exception is socket is closed while waiting for connection
				Socket clientSocket = serverSocket.accept();
				System.out.println("SERVER: client connected");
				//a thread to handle a client
				ServerThread st1 = new ServerThread(model, clientSocket);
				pool.execute(st1);
				clients.add(st1);
			}
		} catch (SocketException e) {
			if(!stop) {//an exception is expected if socket is closed while waiting for connection
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			closeSocket();
		}
	}

	public void stop(){

		for( ServerThread st : clients) {
			st.stopServerTread();
		}
		stop = true;
		pool.shutdown();
		closeSocket();
	}

	public void start(){
		new Thread(()->runServer()).start();
	}

	private void closeSocket() {

		try {
			if(serverSocket != null) {
				serverSocket.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}

class ServerThread extends Thread {

	private Socket socket = null;
	private  boolean stop;
	private final Model model;

	public ServerThread(Model model ,Socket socket) {
		this.model = model;
		this.socket = socket;
	}

	@Override
	public void run() {

		try{
			stop = false;
			DataInputStream in = new DataInputStream( socket.getInputStream() );
			String fromClient;
			while(! stop){
				if((fromClient = in.readUTF()) != null) {
					model.addMessage(fromClient);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void stopServerTread(){
		stop = true;
	}
}
